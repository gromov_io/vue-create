const fse = require('fs-extra')
const _ = require('lodash')
const path = require('path')
const colors = require('colors')
const arg = _.filter(process.argv, item => item !== process.argv[0] && item !== process.argv[1] && item !== process.argv[2])
const originalType = process.argv[2]
const type =  {
	isComponnent: /component/gi.test(originalType),
	isPage: /page/gi.test(originalType),
	isStore: /store/gi.test(originalType)
}
const currentTypePath = () => {
	if (type.isComponnent) return componentsDir
	if (type.isPage) return pagesDir
	if (type.isStore) return storeDir
}

const componentsDir = path.join(__dirname, 'components')
const pagesDir = path.join(__dirname, 'pages')
const storeDir = path.join(__dirname, 'store/modules')

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Файлы компонента
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const componentFiles = (name) => {
	return [
		// Компонент
		{
			name: `index.vue`,
			content: `
				<template>
					<div class="${name}" v-loading="loading">
						Компонтент: ${name}
					</div>
				</template>

				<script src="./${name}.js"></script>
				<style lang="stylus" src="./${name}.styl"></style>
				`.replace(/	{4,4}/gi, '').replace(/\n/, ''),
			path: ''
		},
		// js
		{
			name: `${name}.js`,
			content: `
				export default {
					name: '${name}',
					data () {
						return {
							loading: false
						}
					}
				}
				`.replace(/	{4,4}/gi, '').replace(/\n/, ''),
			path: ''
		},
		// Стили
		{
			name: `${name}.styl`,
			content: `.${name}\n\t//\n`,
			path: ''
		}
	]
}

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Файлы страницы
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const pageFiles = (name) => {
	return [
		// Компонент
		{
			name: `index.vue`,
			content: `
				<template>
					<div class="${name}" v-loading="loading">
						Страница: ${name}
					</div>
				</template>

				<script src="./${name}.js"></script>
				<style lang="stylus" src="./${name}.styl"></style>
				`.replace(/	{4,4}/gi, '').replace(/\n/, ''),
			path: ''
		},
		// js
		{
			name: `${name}.js`,
			content: `
				export default {
					name: '${name}',
					data () {
						return {
							loading: false
						}
					}
				}
				`.replace(/	{4,4}/gi, '').replace(/\n/, ''),
			path: ''
		},
		// Стили
		{
			name: `${name}.styl`,
			content: `.${name}\n\t//\n`,
			path: ''
		}
	]
}

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Файлы Vuex модуля
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const storeFiles = (name) => {
	return [
		{
			name: `${name}.js`,
			content: `
				export default {
					state:{
						items: null,
					},
					mutations: {
						SET_${name.toUpperCase()} (state, data) {
							state.items = data
						}
					},
					actions:{
						get_${name} ({commit}, data) {
							commit('SET_${name.toUpperCase()}', data)
						},
					}
				}
				`.replace(/	{4,4}/gi, '').replace(/\n/, ''),
			path: ''
		}
	]
}

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// Проверяем что-бы не перезаписать
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
arg.forEach(async originalPath => {
	const dataInfo = {
		name: _.last(originalPath.split('/')),
		path: path.join(currentTypePath(), originalPath),
		originalPath
	}
	try {
		const exists = await fse.pathExists(dataInfo.path)
		if (exists) throw 'exists'
		switch (true) {
			case type.isComponnent:
				createComponent(dataInfo)
				break
			case type.isPage:
				createPage(dataInfo)
				break
			case type.isStore:
				createStor(dataInfo)
				break
			default:
				console.log('Что-то пошло не так')
		}

	} catch (error) {
		if (error === 'exists') {
			let typeName = type.isComponnent ? 'Компонтент' : type.isPage ? 'Страница' : 'Vuex модуль'
			console.log(colors.red(`${typeName}: ${dataInfo.name} уже существует`))
		}
	}
})

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Cоздания компонента
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const createComponent = (component) => {
	fse.mkdirp(component.path)
	componentFiles(component.name).forEach( async item => {
		if (item.isFolder) {
			fse.mkdirp(path.join(component.path, item.path, item.name))
		} else {
			try {
				const fille = path.join(component.path, item.path, item.name)
				await fse.outputFile(fille, item.content)
			} catch (error) {
				console.log(colors.red(`При создании файла: ${item.name} - произошла неизвесная ошибка ((.`))
			}
		}
	})
	console.log(colors.green(`Компонент: ${component.name} - успешно создан.`))
}

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Cоздания страницы
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const createPage = (page) => {
	fse.mkdirp(page.path)
	pageFiles(page.name).forEach( async item => {
		if (item.isFolder) {
			fse.mkdirp(path.join(page.path, item.path, item.name))
		} else {
			try {
				const fille = path.join(page.path, item.path, item.name)
				await fse.outputFile(fille, item.content)
			} catch (error) {
				console.log(colors.red(`При создании файла: ${item.name} - произошла неизвесная ошибка ((.`))
			}
		}
	})
	console.log(colors.green(`Страница: ${page.name} - успешно создана.`))
}

// ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
// 	Cоздания Vuex модуля
// ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱ ̱
const createStor = (store) => {
	storeFiles(store.name).forEach( async item => {
		try {
			const exists = await fse.pathExists(path.join(storeDir, item.name))
			if (exists) throw 'exists'
			const fille = path.join(storeDir, item.name)
			await fse.outputFile(fille, item.content)
			console.log(colors.green(`Vuex модуль: ${store.name} - успешно создан.`))
		} catch (error) {
			if (error === 'exists') {
				console.log(colors.red(`Vuex модуль: ${store.name} - уже существует.`))
			} else {
				console.log(colors.red(`При создании Vuex модуля: ${store.name} - произошла неизвесная ошибка ((.`))
			}
		}
	})
}